;2X2 GRID DOT GAME
;@AUTHOR DAWOODUL ISLAM and RADWA KAIKAUS


jumps
.model small
.data 

  
temp1 dw 0
temp2 dw ? 
temp3 dw 0
temp dw ?
player1point db 0

player2point db 0 

msg1 db "         No one is winner$"
msg2 db "         Player 1 is winner POINT:  $"  
msg3 db "         Player 2 is winner POINT:  $" 
blank db 0dh,0ah,'$'


b1 dw 0
b2 dw 0
b3 dw 0
b4 dw 0

user db 0
c db 0


.code 
pushall macro
    push ax
    push bx
    push cx
    push dx
endm

popall macro
    pop dx
    pop cx
    pop bx
    pop ax
endm


draw_point macro x, y    
    
    pushall
    mov cx, word ptr x
    mov dx, word ptr y
    mov ah, 0ch
    mov al, 0fh
    mov bx, 0
    int 10h
    popall
endm 
H_line macro  a, b, c
    LOCAL label1
    pushall  
    mov cx, word ptr a
    mov dx, word ptr b 
    mov bx, word ptr c 
    add cx,bx
    mov temp,cx 
    mov cx, word ptr a
    label1:
     
    
     mov ah, 0ch
     mov al, 13
     int 10h
                       
     inc cx
                       
     cmp cx,temp
     jle label1
    
    
    
    popall
endm  
V_line macro  a, b, c
    LOCAL label2
    pushall  
    mov cx, word ptr a
    mov dx, word ptr b 
    mov bx, word ptr c 
    add dx,bx
    mov temp2,dx 
    mov dx, word ptr b
    label2:
     
    
     mov ah, 0ch
     mov al, 13
     int 10h
                       
     inc dx
                       
     cmp dx,temp2
     jle label2
    
    
    
    popall
endm




proc main 
mov dx, @data
mov ds, dx

mov ah, 0         ;graphics mode set
mov al, 12h       ;640*480,16 colours
int 10h  

mov ax, 0
int 33h             ;for mouse


draw_point 10,20   
draw_point 50,20  
draw_point 90,20  
draw_point 10,60  
draw_point 50,60  
draw_point 90,60

draw_point 10,100  
draw_point 50,100   
draw_point 90,100

;L3 for mouse read and check loop
L3:
  mov ax, 1    ;show mouse pointer
  int 33h

  mov ax, 3
  int 33h       ;get mouse position and status of its buttons
 
  cmp bx, 1        ;mouse click check
  je draw          ;draw line
  
  cmp b1, 4       ; print
  jge box1
  
  cmp b2, 4
  jge box2
  
  cmp b3, 4
  jge box3
  
  cmp b4, 4
  jge box4
  
  cmp c, 4
  je exit        ;exit after print in 4 boxes
  
  jmp L3
      
      draw: 
        
        cmp dx ,20      ;row one                                             
         
        je drw1                                                              
                                                                    
        cmp dx, 60       ;row2                                               
     
        je drw2                                                             
                                                                     
        cmp dx, 100       ;row3                                              
     
        je drw3
                                                    
        cmp cx, 10      ;column1                                             
        
        je ydrw1                                                             
                                                                             
        cmp cx, 50        ;column2                                           
        
        je ydrw22
                                                                    
        cmp cx, 90         ;column 3                                         
         
        je ydrw33
                                                                    
        jmp L3                                                               
            drw1:                                                            
               cmp cx, 50                                                    
               jl loop1       ;r11                                           
    
               jg loop2        ;r12                                          
    
                                                                  
               jmp L3                                                        
   
               loop1:                                                   
                                                                             
               cmp cx, 11                                            
               jge L                                                 
               L:                     ;r11                                   
                             
               H_line 11,20,38
               inc b1
               mov ah, 1
               int 21h
               mov user, al           ;user 1 or 2                        
               jmp L3                                       
                                                                             
               loop2:                 ;r12                  
               cmp cx, 89                                            
               jle LL                                                
               LL:                                                   
               H_line 51,20, 38                                              
   
               inc b2
               mov ah, 1
               int 21h
               mov user, al             ;user 1 or 2                         
          
                                                                   
               jmp L3                                                        
                                           
                                                                             
               drw2:                                                    
               cmp cx, 50                                            
               jl loop3      ;r21                                        
               jg loop4     ;r22                                         
                                                                   
               jmp L3                                                   
               loop3:                                        
                                                                             
               cmp cx, 11                               
               jge LLL                                  
               LLL:                                     
               H_line 11, 60, 38   ;r21                                  
               inc b1                                   
               inc b3                                   
               mov ah, 1
               int 21h
               mov user, al       ;user 1 or 2                               
              
               jmp L3                                   
                                                                             
               loop4:                                        
               cmp cx, 89                               
               jle LLLL                                 
               LLLL:                ;r22                    
               H_line 51,60,38                                
               inc b2                                   
               inc b4
               mov ah, 1
               int 21h
               mov user, al        ;user 1 or 2                            
                ;inc user                               
                jmp L3                                   
                                                                             
               drw3:                                                     
                   cmp cx, 50                                            
                   jl loop5              ;r31                                
                   jg loop6               ;r32                               
                                                                             
                   loop5:                                        
                        cmp cx, 11                               
                        jge LEEE                                 
                        LEEE:                 ;r31                   
                        H_line 11, 100, 38                                 
                        inc b3                                   
                        mov ah, 1
                        int 21h
                        mov user, al    ;user 1 or 2                         
    
                        jmp L3                                   
                                                                 
                   loop6:                                        
                        cmp cx, 89                               
                        jle LO                                   
                        LO:                    ;r32                  
                        H_line 51, 100, 38                                  
                        inc b4 
                        mov ah, 1
                        int 21h
                        mov user, al          ;user 1 or 2                   
               
                        jmp L3                                   
                                                                             
                                                                             
                        ydrw1:                ;column1                       
                            cmp dx, 60                               
                            jl lp1             ;c11                      
                            jg lp2             ;c12                      
                            jmp L3                                       
                                 lp1:                                
                                    cmp dx, 21                       
                                    jge LQ                           
                                    LQ:                ;c11              
                                    V_line 10,21,38                          
 
                                    inc b1 
                                    mov ah, 1
                                    int 21h
                                    mov user, al        ;user 1 or 2         
          
                                                       
                                    jmp L3                           
                                                                     
                                  lp2:               ;c12                
                                  cmp dx, 99                         
                                  jle LW                             
                                                                     
                                  LW:                  ;c12              
                                  V_line 10,61,38                            
 
                                  inc b3                             
                                  mov ah, 1
                                  int 21h
                                  mov user, al            ;user 1 or 2       
    
                                  jmp L3                             
                                                                             
                                                   
                            ydrw22:                ;clumn2              
                                cmp dx, 60                       
                                jl lp3               ;c21            
                                jg lp4               ;c22            
                                jmp L3                               
                                                                 
                                    lp3:                         
                                    cmp dx, 21                   
                                    jge LR                       
                                    LR:                ;c21          
                                    V_line 50,21,38                       
                                    inc b1                       
                                    inc b2
                                    mov ah, 1
                                    int 21h
                                    mov user, al       ;user 1 or 2          
       
                                                      
                                    jmp L3                       
                                                                 
                                    lp4:                         
                                    cmp dx, 99                   
                                    jle LTt                      
                                    LTt:               ;c22          
                                    V_line 50,61,38                      
                                    inc b3                       
                                    inc b4
                                    mov ah, 1 
                                    int 21h
                                    mov user, al          ;user 1 or 2       
       
                                    ;inc user                   
                                    jmp L3                       
                                                                             
                          ydrw33:                                
                              cmp dx, 60                         
                              jl lp5            ;c31                 
                              jg lp6            ;c32                 
                                                                 
                                    lp5:                         
                                    cmp dx, 21                   
                                    jge LY                       
                                    LY:               ;c31           
                                    V_line 90,21,38                         
                                    inc b2
                                    mov ah, 1
                                    int 21h
                                    mov user, al    ;user 1 or 2             
       
                                                   
                                    jmp L3                       
                                                                 
                                    lp6:               ;c32          
                                    cmp dx, 99                   
                                    jle LU                       
                                    LU:               ;c32           
                                    V_line 90,61,38                       
                                    inc b4                       
                                    mov ah, 1
                                    int 21h
                                    mov user, al    ;user 1 or 2 
                                    jmp L3
                  ;box1 is for print afer complete                  
                  box1:                                                      
                                                     
                   mov b1, 0        ;stop repeatation                        
              
                   cmp user, 31h                                             
                                                                             
                              
                   je plusprint      ;user 1                                 
                                                 
                    jne minusprint    ;user2               
                      plusprint:                     
                      mov cx, 20                     
                         Lp:                         
                            mov ah, 0ch              
                            mov al, 4                
                                                     
                            mov dx, 40               
                            int 10h         ;print--         
                                                     
                            inc cx                   
                            cmp cx, 40               
                            jle Lp                   
                       mov dx, 30                    
                            Lp11:                    
                              mov ah, 0ch            
                              mov al, 4              
                                                     
                              mov cx, 30      ;print |       
                              int 10h                
                                                     
                              inc dx                 
                              cmp dx, 50             
                              jle Lp11               
                              inc c        ;Box print count
                              
                              inc player1point 
                              jmp L3                 
                       minusprint:                   
                                                     
                       mov cx, 20                    
                       ML:                           
                       mov ah, 0ch                   
                       mov al, 6                     
                       mov dx, 40                    
                       int 10h                ;print--         
                       inc cx                        
                       cmp cx, 40                    
                       jle ML                        
                       inc c     ;Box print count 
                       inc player2point                    
                       jmp L3                        
                  ;box2 is for print afer complete                           
        
                 box2:                               
                                                     
                    mov b2, 0                        
                    cmp user, 31h                   
                                    
                    je plusprint1                    
                    jne minusprint1                  
                      plusprint1:                    
                      mov cx, 60                     
                         Lpp:                        
                            mov ah, 0ch              
                            mov al, 4                
                                                     
                            mov dx, 40               
                            int 10h           ;print--            
                                                     
                            inc cx                   
                            cmp cx, 80               
                            jle Lpp                  
                       mov dx, 30                    
                            Lpp11:                   
                              mov ah, 0ch            
                              mov al, 4              
                                                     
                              mov cx, 70             
                              int 10h           ;print |       
                                                     
                              inc dx                 
                              cmp dx, 50             
                              jle Lpp11              
                              inc c     ;Box print count
                              inc player1point             
                              jmp L3                 
                       minusprint1:                  
                       ;mov dx, 60                   
                       mov cx, 60                    
                       ML1:                          
                       mov ah, 0ch                   
                       mov al, 6                     
                       mov dx, 40            ;print--          
                       int 10h                       
                       inc cx                        
                       cmp cx, 80                    
                       jle ML1                       
                       inc c            ;Box print count
                       inc player2point              
                       jmp L3                        
                 ;box3 is for print afer complete
                 box3:                               
                                                     
                   mov b3, 0                         
                    cmp user, 31h                   
                    ;cmp al, 1                       
                    ;mov b3, 0                       
                    ;cmp user, 31h                  
                    je plusprint2                    
                    jne minusprint2                  
                      plusprint2:                    
                      mov cx, 20                     
                      ;mov dx, 60                    
                         Lppp:                       
                            mov ah, 0ch              
                            mov al, 4                
                                                     
                            mov dx, 80               
                            int 10h              ;print |      
                                                     
                            inc cx                   
                            cmp cx, 40               
                            jle Lppp                 
                       mov dx, 70                    
                            Lpp111:                  
                              mov ah, 0ch            
                              mov al, 4              
                                                     
                              mov cx, 30             
                              int 10h           ;print --      
                                                     
                              inc dx                 
                              cmp dx, 90             
                              jle Lpp111             
                              inc c       ;Box print count 
                              inc player1point          
                              jmp L3                 
                       minusprint2:                  
                       ;mov dx, 60                   
                       mov cx, 20                    
                       ML11:                         
                       mov ah, 0ch                   
                       mov al, 6                     
                       mov dx, 80                    
                       int 10h                ;print --         
                       inc cx                        
                       cmp cx, 40                    
                       jle ML11                      
                       inc c           ;Box print count
                       inc player2point               
                       jmp L3
                ;box4 is for print afer complete        
                box4:                                
                                                     
                    mov b4, 0                        
                    cmp user, 31h                   
                    ;cmp al, 1                       
                    ;mov b4, 0                       
                    ;cmp user, 31h                  
                    je plusprint3                    
                    jne minusprint3                  
                      plusprint3:                    
                      mov cx, 60                     
                      ;mov dx, 60                    
                         Lppp1:                      
                            mov ah, 0ch              
                            mov al, 4                
                                                     
                            mov dx, 80               
                            int 10h                  
                                                     
                            inc cx                   
                            cmp cx, 80               
                            jle Lppp1                
                       mov dx, 70                    
                            Lppp111:                 
                              mov ah, 0ch            
                              mov al, 4              
                                                     
                              mov cx, 70             
                              int 10h                
                                                     
                              inc dx                 
                              cmp dx, 90             
                              jle Lppp111            
                              inc c          ;Box print count 
                              inc player1point       
                              jmp L3                 
                       minusprint3:                  
                       ;mov dx, 60                   
                       mov cx, 60                    
                       ML111:                        
                       mov ah, 0ch                   
                       mov al, 6                     
                       mov dx, 80                    
                       int 10h                       
                       inc cx                        
                       cmp cx, 80                    
                       jle ML111                     
                       inc c          ;Box print count 
                       inc player2point              
                       jmp L3         
                       

exit:     


mov al,player1point
cmp al,player2point   
je print1
jg print2
jl print3   

print1:  
     lea dx,msg1
     mov ah,9
     int 21h 
     jmp exitt 
     
    
      


print2:  
     lea dx,msg2
     mov ah,9
     int 21h 
     mov dl, player1point 
     add dl,30h
     mov ah,2
     int 21h 
     
     jmp exitt 


print3:  
     lea dx,msg3
     mov ah,9
     int 21h
     mov dl, player2point 
     add dl,30h
     mov ah,2
     int 21h 
     jmp exitt 

exitt:
mov ah, 4ch
int 21h
endp main
;end main




end main   ;last line of file                                                
           
     ;last line of file                                                           
  